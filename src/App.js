import React from "react";
import "./App.css";
import { HashRouter, Router, Redirect, Route, Switch } from "react-router-dom";
import AcreTool from "./AcreTool";
import MainMenu from "./MainMenu";
import MapTracking from "./MapTracking";
import { createBrowserHistory } from "history";

const history = createBrowserHistory();

function App() {
  return (
    <div className="App">
      <HashRouter history={history}>
        <Switch>
          <Route component={AcreTool} path="/acretool"></Route>
          <Route component={MainMenu} path="/mainmenu"></Route>
          <Route component={MapTracking} path="/maptracking"></Route>
          <Route component={MainMenu} exact path="/"></Route>
        </Switch>
      </HashRouter>
    </div>
  );
}

export default App;
