import React, { Component } from "react";

import "leaflet/dist/leaflet.css";

import "./App.css";

import L from "leaflet";
import "leaflet-draw";
import "leaflet-draw/dist/leaflet.draw.css";

import "leaflet-geometryutil";
import { Button } from "@material-ui/core";

import homeicon from "./homeicon.svg";
import starticon from "./starticon.svg";

import { SphericalUtil, PolyUtil } from "node-geometry-library";

var attributionStr =
  "Esri, DigitalGlobe, GeoEye, i-cubed, USDA FSA, USGS, AEX, Getmapping, Aerogrid, IGN, IGP, swisstopo, and the GIS User Community";

var map;

var polyLineList = [];
var track = false;
var initialData = "empty";
var currentLat;
var currentLon;
var previousLat;
var previousLon;
var firstpolyline;
var trackingPolygonPoints;
var trackingPolygon;

var polygonCounter = 0;

class MapTracking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lng: -82.6,
      lat: 39.7,
      zoom: 12,
      currentLocation: { lat: 39.723464, lng: -82.5993 },
      gpsData: "",
    };
  }

  componentDidMount() {
    var self = this;

    window.ipcRenderer.on("gpsinfo", (event, message) => {
      // console.log(message);

      initialData = message;

      currentLat = message.lat;
      currentLon = message.lon;

      this.setState({ gpsData: message });

      //don't store repeat locations.
      if (currentLat === previousLat && currentLon === previousLon) {
      } else {
        if (track === true) {
          polyLineList.push(new L.LatLng(message.lat, message.lon));
        }
      }

      previousLat = currentLat;
      previousLon = currentLon;
    });

    console.log(initialData);

    if (initialData === "empty") {
      self.activateMap(38.910559, -83.439735);
    } else {
      self.activateMap(initialData.lat, initialData.lon);
    }

    //https://www.npmjs.com/package/leaflet.tilelayer.pouchdbcached
    //https://github.com/MazeMap/Leaflet.TileLayer.PouchDBCached
    //https://leafletjs.com/plugins.html
  }

  activateMap = (lat, long) => {
    var self = this;

    map = L.map("map").setView([lat, long], 13);

    L.tileLayer(
      "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
      {
        useCache: true,
        crossOrigin: true,
        attribution: attributionStr,
      }
    ).addTo(map);

    L.tileLayer(
      "https://server.arcgisonline.com/ArcGIS/rest/services/Reference/World_Transportation/MapServer/tile/{z}/{y}/{x}",
      {
        useCache: true,
        crossOrigin: true,
        attribution: attributionStr,
      }
    ).addTo(map);

    setTimeout(function () {
      if (initialData === "empty") {
      } else {
        map.setView([initialData.lat, initialData.lon], 17);
      }
    }, 3000);
  };

  drawPolyline = () => {
    track = true;

    firstpolyline = new L.Polyline(polyLineList, {
      color: "red",
      weight: 3,
      opacity: 0.5,
      smoothFactor: 1,
    });
    firstpolyline.addTo(map);
    /*
    var secondPolyLineList = [new L.LatLng(38.9091890969817, -83.44073295593263), new L.LatLng(38.909427030614665,-83.44048082828523), new L.LatLng(38.909681660461246,-83.44018042087556), new L.LatLng(38.90994046363161,-83.43992292881013), new L.LatLng(38.910291098679764,-83.43948304653169)]

   var secondpolyline = new L.Polyline(secondPolyLineList, {
        color: 'red',
        weight: 8,
        opacity: 0.5,
        smoothFactor: 1
    });
    secondpolyline.addTo(map);
*/
  };

  goHome = () => {
    this.props.history.push("/mainmenu");
  };

  drawPolygon = () => {
    //create a list of headings calculated from the list of points

    var headingArr = [];

    let samplePointList = [
      new L.LatLng(25.775, -80.19),
      new L.LatLng(21.774, -80.19),
    ];

    console.log(map._layers);

    //trackingPolygon = null;
    if (polygonCounter > 2) {
      map.removeLayer(trackingPolygon);
    }

    let leftArr = [];
    let rightArr = [];

    trackingPolygonPoints = [];

    if (polyLineList.length > 2) {
      polygonCounter++;

      for (var p = 0; p < polyLineList.length - 1; p++) {
        let instantHeading = SphericalUtil.computeHeading(
          polyLineList[p],
          polyLineList[p + 1]
        );
        console.log(instantHeading);

        headingArr.push(instantHeading);

        if (p == 0) {
          //duplicate heading at beginning of list since there will be 1 less heading than points.

          headingArr.push(instantHeading);
        }

        //compute point to the right and to the left at X meters

        let pointsToLeft = SphericalUtil.computeOffset(
          polyLineList[p],
          3,
          instantHeading + 90
        );

        leftArr.push(pointsToLeft);

        let pointsToRight = SphericalUtil.computeOffset(
          polyLineList[p],
          3,
          instantHeading + 270
        );

        rightArr.push(pointsToRight);
      }

      //combine the left and right array to make a whole polygon

      trackingPolygonPoints = leftArr.concat(rightArr);

      trackingPolygon = L.polygon(trackingPolygonPoints, { color: "red" });
      trackingPolygon.addTo(map);
    }
  };

  render() {
    console.log(polyLineList);
    /*

*/
    //constantly update map with polygon and present location
    if (track === true) {
      //  map.setView([this.state.gpsData.lat, this.state.gpsData.lon]);
      /*
            firstpolyline = new L.Polyline(polyLineList, {
                color: 'red',
                weight: 3,
                opacity: 0.5,
                smoothFactor: 1
            });
            firstpolyline.addTo(map);
*/
      this.drawPolygon();
    }

    return (
      //TODO OK MAYBE WE DOWNLOAD DIRECTLY FROM OSM. and use https://openmaptiles.org/docs/host/tileserver-gl/ skip the openmaptiles server.
      //I think switch2osms docker tutorial may be best. completely free and open source. screw openmaptiles https://switch2osm.org/serving-tiles/using-a-docker-container/
      <div>
        <div id="map" />

        <Button
          onClick={() => this.drawPolyline()}
          style={{
            position: "absolute",
            width: "25px",
            left: "20px",
            bottom: "150px",
            zIndex: 999,
          }}
        >
          <img src={starticon} />
        </Button>

        <Button
          onClick={() => this.goHome()}
          style={{
            position: "absolute",
            width: "25px",
            left: "20px",
            bottom: "10px",
            zIndex: 999,
          }}
        >
          <img src={homeicon} />
        </Button>
      </div>
    );
  }
}

export default MapTracking;
