import React, { Component } from "react";

import "leaflet/dist/leaflet.css";

import "./App.css";

import L from "leaflet";

import "leaflet-draw";
import "leaflet-draw/dist/leaflet.draw.css";

import "leaflet-geometryutil";
import { Button } from "@material-ui/core";
import homeicon from "./homeicon.svg";

var attributionStr =
  "Esri, DigitalGlobe, GeoEye, i-cubed, USDA FSA, USGS, AEX, Getmapping, Aerogrid, IGN, IGP, swisstopo, and the GIS User Community";
var map;
var initialData = "empty";

class AcreTool extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lng: -82.6,
      lat: 39.7,
      zoom: 12,
      areaOfLastPoly: 0,
      currentLocation: { lat: 39.723464, lng: -82.5993 },
    };
  }

  componentDidMount() {
    var self = this;

    window.ipcRenderer.on("gpsinfo", (event, message) => {
    //  console.log(message);

      initialData = message;

      this.setState({ gpsData: message });
    });

    console.log(initialData);

    if (initialData === "empty") {
      self.activateMap(38.910559, -83.439735);
    } else {
      self.activateMap(initialData.lat, initialData.lon);
    }

    //https://www.npmjs.com/package/leaflet.tilelayer.pouchdbcached
    //https://github.com/MazeMap/Leaflet.TileLayer.PouchDBCached
    //https://leafletjs.com/plugins.html
  }

  activateMap = (lat, long) => {
    var self = this;

    map = L.map("map").setView([lat, long], 13);

    //refresh the first time if GPS hadn't been updated

    setTimeout(function () {
      if (initialData === "empty") {
      } else {
        map.setView([initialData.lat, initialData.lon], 13);
      }
    }, 3000);

    L.tileLayer(
      "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
      {
        useCache: true,
        crossOrigin: true,
        attribution: attributionStr,
      }
    ).addTo(map);

    L.tileLayer(
      "https://server.arcgisonline.com/ArcGIS/rest/services/Reference/World_Transportation/MapServer/tile/{z}/{y}/{x}",
      {
        useCache: true,
        crossOrigin: true,
        attribution: attributionStr,
      }
    ).addTo(map);

    var drawnItems = new L.FeatureGroup();
    map.addLayer(drawnItems);

    var drawControl = new L.Control.Draw({
      draw: {
        polygon: true,
        marker: false,
        circlemarker: false,
        circle: false,
      },
    });
    map.addControl(drawControl);

    map.on("draw:created", function (e) {
      console.log("created");

      console.log(e);
      console.log(e.layer.getLatLngs());

      var areaofPoly = L.GeometryUtil.geodesicArea(e.layer.getLatLngs()[0]);

      console.log(areaofPoly * 0.000247105);

      self.setState({ areaOfLastPoly: areaofPoly * 0.000247105 });

      console.log(e.layer._latlngs);

      self.drawPolygon(e.layer._latlngs);
    });
  };

  drawPolygon = (coords) => {
    //var coords =  [[48,-3],[50,5],[44,11],[48,-3]] ;

    var a = coords;

    var polygon = L.polygon(a, { color: "red" });
    polygon.addTo(map);

    map.fitBounds(polygon.getBounds());

    //let area = L.

    //console.log(area);
  };

  goHome = () => {
    this.props.history.push("/mainmenu");
  };

  render() {
    return (
      //TODO OK MAYBE WE DOWNLOAD DIRECTLY FROM OSM. and use https://openmaptiles.org/docs/host/tileserver-gl/ skip the openmaptiles server.
      //I think switch2osms docker tutorial may be best. completely free and open source. screw openmaptiles https://switch2osm.org/serving-tiles/using-a-docker-container/
      <div>
        <Button
          onClick={() => this.goHome()}
          style={{
            position: "absolute",
            width: "25px",
            left: "20px",
            bottom: "10px",
            zIndex: 999,
          }}
        >
          <img src={homeicon} />
        </Button>

        <h1
          style={{
            position: "fixed",
            top: "50 px",
            right: "50px",
            fontSize: "30px",
            color: "white",
            zIndex: "999",
          }}
        >
          {Math.round(this.state.areaOfLastPoly * 100) / 100} acres
        </h1>
        <div id="map" />
      </div>
    );
  }
}

export default AcreTool;
