// Modules to control application life and create native browser window

const { app, BrowserWindow } = require("electron");
//const electron = require('electron')
//const ipc = electron.ipcMain
const path = require("path");
var GPS = require("gps");
var gps = new GPS();

function runGPS() {
  var sf = require("sf");

  console.log("THIS CODE IS RUNNING");

  var SerialPort = require("serialport");
  var resultsList = [];
  SerialPort.list().then((ports) => {
    ports.forEach(function (port) {
      console.log(port);
      console.log(port.pnpId);
      console.log(port.manufacturer);
    });
  });

  const Readline = require("@serialport/parser-readline");

  var port = new SerialPort("COM4", {
    // change path
    baudRate: 4800,
  });

  const parser = new Readline({
    delimiter: "\r\n",
  });

  // const parser = new Readline()
  port.pipe(parser);

  //parser.on('data', console.log)
  /* port.on('data', function(data) {
    console.log(data)
    gps.updatePartial(data);
  });*/

  parser.on("data", function (data) {
    gps.update(data);
    // console.log(gps.GGAQuality)
    // console.log(gps.state.lat)
    //  console.log(data);
  });
}

function createWindow() {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 480,
    webPreferences: {
      preload: path.join(__dirname, "preload.js"),
    },
  });

  gps.on("data", function (data) {
    // console.log(data, gps.status);

    var gpsInfo = {};

    gpsInfo.speed = gps.state.speed;
    gpsInfo.lat = gps.state.lat;
    gpsInfo.lon = gps.state.lon;
    gpsInfo.satsActive = gps.state.satsActive;
    gpsInfo.time = gps.state.time;
    gpsInfo.alt = gps.state.alt;

    //console.log(gps.state.lat)
    //console.log(gps.state.lon)
    // console.log(gps.state.satsActive)
    // console.log(gps.state.speed)

    mainWindow.webContents.send("gpsinfo", gpsInfo);
  });

  // and load the index.html of the app.
  mainWindow.loadFile("build/index.html");
  //remove menu tool bar
  //mainWindow.removeMenu()

  // Open the DevTools.

  mainWindow.webContents.openDevTools();
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow();
  runGPS();

  app.on("activate", function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", function () {
  if (process.platform !== "darwin") app.quit();
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
