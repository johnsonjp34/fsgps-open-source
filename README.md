# FSGPS on Electron with React

Open Source version of Farm Sprayer GPS. Designed with React to run on Electron so it will work on any modern hardware. See the plantermonitor.com/store site to help support the project by buying hardware.

npm install

npm run build

npm run electron

Mapping is based on Leaflet with maps sourced from ESRI. This project is open source, HOWEVER, if you end up using this for commercial purposes you will need the proper ESRI developer subscription and proper map attributions. Because this project uses Leaflet you can easily substitute for Open Street Maps etc.